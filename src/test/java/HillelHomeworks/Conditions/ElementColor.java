package HillelHomeworks.Conditions;

import com.codeborne.selenide.CheckResult;
import com.codeborne.selenide.Driver;
import com.codeborne.selenide.WebElementCondition;
import org.openqa.selenium.WebElement;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElementColor extends WebElementCondition {
    private Color color;

    public ElementColor(Color color) {
        super("element color");
        this.color = color;
    }

    @Nonnull
    @CheckReturnValue
    public CheckResult check(Driver driver, WebElement element) {
        CustomColor elementColor = getElementColor(element);
        boolean hasRightColor = elementColor.equals(color);
        return new CheckResult(hasRightColor, String.format("color=\"%s\"", elementColor));
    }

    public CustomColor getElementColor(WebElement element) {
        return getColorFromCss(element.getCssValue("background-color"));
    }

    public CustomColor getColorFromCss(String cssValue) {
        List<Integer> rgb = parseRGB(cssValue);
        return new CustomColor(rgb.get(0), rgb.get(1), rgb.get(2));
    }

    public List<Integer> parseRGB(String rgbColors) {
        return parseRGBA(rgbColors).subList(0, 3)
                .stream().map(Number::intValue)
                .toList();
    }

    private List<Float> parseRGBA(String rgbColors) {
        List<Float> rgbColorCode = new LinkedList<>();

        Pattern pattern = Pattern.compile("[0-9.]+");
        Matcher matcher = pattern.matcher(rgbColors);
        while (matcher.find()) {
            rgbColorCode.add(Float.parseFloat(matcher.group()));
        }
        return rgbColorCode;
    }

    @Nonnull
    @CheckReturnValue
    public String toString() {
        return String.format("%s \"%s\"", this.getName(), this.color);
    }
}
