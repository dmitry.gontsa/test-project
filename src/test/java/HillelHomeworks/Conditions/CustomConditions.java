package HillelHomeworks.Conditions;

import com.codeborne.selenide.WebElementCondition;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;
import java.awt.*;

public class CustomConditions {
    @CheckReturnValue
    @Nonnull
    public static WebElementCondition color(Color expectedColor) {
        return new ElementColor(expectedColor);
    }
}
