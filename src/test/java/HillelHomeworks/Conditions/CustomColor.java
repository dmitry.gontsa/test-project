package HillelHomeworks.Conditions;

import java.awt.*;

public class CustomColor extends Color {
    public final static CustomColor SUMMER_SKY = new CustomColor(43, 166, 203);
    public final static CustomColor VENETIAN_RED = new CustomColor(198, 15, 19);
    public final static CustomColor DARK_GREEN = new CustomColor(93, 164, 35);

    public CustomColor(int r, int g, int b) {
        super(r, g, b);
    }
}
