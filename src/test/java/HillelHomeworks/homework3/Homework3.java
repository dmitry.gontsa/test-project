package HillelHomeworks.homework3;

import HillelHomeworks.BaseTest;
import HillelHomeworks.Commands.CustomCloseCommand;
import org.testng.annotations.Test;

import static HillelHomeworks.Conditions.CustomColor.*;
import static HillelHomeworks.Conditions.CustomConditions.color;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

public class Homework3 extends BaseTest {
    private static final String ENTRY_AD = "/entry_ad";
    private static final String CHALLENGING_DOM = "/challenging_dom";

    @Test
    void customCommandTest() {
        open(ENTRY_AD);
        $(".modal-footer > p").execute(new CustomCloseCommand());
    }

    @Test
    void customConditionTest() {
        open(CHALLENGING_DOM);
        $$(".large-2 .button").first().shouldBe(color(SUMMER_SKY));
    }
}
