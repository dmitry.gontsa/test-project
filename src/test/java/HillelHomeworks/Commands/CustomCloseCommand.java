package HillelHomeworks.Commands;

import com.beust.jcommander.internal.Nullable;
import com.codeborne.selenide.Command;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.impl.WebElementSource;

import static com.codeborne.selenide.ClickOptions.withTimeout;
import static com.codeborne.selenide.Selenide.$;
import static java.sql.DriverManager.println;
import static java.time.Duration.ofSeconds;

public class CustomCloseCommand implements Command<SelenideElement> {
    @Override
    public SelenideElement execute(SelenideElement selenideElement, WebElementSource webElementSource, @Nullable Object[] objects) {
        webElementSource.getWebElement().click();
        $(".example #restart-ad").doubleClick()
                .click(withTimeout(ofSeconds(100)));
        try {
            webElementSource.getWebElement().click();
        } catch (Exception e) {
            println(e.getMessage());
        }
        return selenideElement;
    }
}
