package HillelHomeworks.homework2;

import HillelHomeworks.BaseTest;
import HillelHomeworks.ChromeDriverSetup;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.dismiss;

import org.openqa.selenium.chrome.ChromeDriver;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;
import static com.codeborne.selenide.selector.ByDeepShadow.cssSelector;
import static java.time.Duration.ofSeconds;
import static org.testng.Assert.assertEquals;

public class Homework2 extends BaseTest {

    private By message = cssSelector("#message");
    private By preloader = cssSelector("#loading");
    private By checkbox = cssSelector("#checkbox input");
    private By inputField = cssSelector("#input-example input");
    private By exampleButton = cssSelector("#input-example button");
    private static final String DYNAMIC_CONTROLS = "/dynamic_controls";
    private static final String TEST_STRING = "Test string";

    @Test
    void firefoxTest() {
        Configuration.browser = "firefox";
        open(DYNAMIC_CONTROLS);

        $(inputField).shouldBe(disabled);
        $(exampleButton).click();

        $(inputField).shouldBe(enabled).setValue(TEST_STRING);
        assertEquals(TEST_STRING, $(inputField).attr("value"));

        $(exampleButton).click();
        $(inputField).shouldBe(disabled);
        assertEquals("It's disabled!", $(message).text());
    }

    @Test
    void chromeTest() {
        setWebDriver(new ChromeDriver());
        open(DYNAMIC_CONTROLS);

        $(checkbox).shouldBe(exist, enabled);
        $(checkbox).shouldBe(not(checked));
        $(checkbox).click();
        $(checkbox).shouldBe(checked);

        $(byText("Remove")).click();
        $(preloader).shouldBe(visible).shouldNotBe(visible, ofSeconds(10));
        $(checkbox).shouldNot(exist);

        $(byText("Add")).click();
        $(preloader).shouldNotBe(visible);

        $("#checkbox").should(exist);
        assertEquals("It's back!", $(message).text());
        closeWebDriver();
    }

    @Test
    void webDriverProviderTest() {
        Configuration.browser = ChromeDriverSetup.class.getName();
        open("/javascript_alerts");

        $$("button")
                .filter(exactText("Click for JS Alert"))
                .first().click();

        dismiss("I am a JS Alert");
        assertEquals("You successfully clicked an alert", $("#result").text());
    }
}
