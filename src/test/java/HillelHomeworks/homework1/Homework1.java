package HillelHomeworks.homework1;

import HillelHomeworks.BaseTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.ModalOptions.withExpectedText;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.dismiss;
import static com.codeborne.selenide.Selenide.prompt;
import static com.codeborne.selenide.Selenide.confirm;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static org.testng.Assert.assertEquals;

public class Homework1 extends BaseTest {

    private static final String RESULT = "#result";
    private static final String JS_CONFIRM = "I am a JS Confirm";
    private static final String CLICK_JS_CONFIRM = "Click for JS Confirm";
    private static final String CLICKED_OK = "You clicked: Ok";

    @BeforeTest
    void setUp() {
        open("/javascript_alerts");
    }

    @Test
    void confirmAlertTest() {
        $(byText("Click for JS Alert")).click();
        dismiss(withExpectedText("I am a JS Alert"));
        assertEquals("You successfully clicked an alert", $(RESULT).text());
    }

    @Test
    void dismissAlertTest() {
        $(byText(CLICK_JS_CONFIRM)).click();
        dismiss(JS_CONFIRM);
        assertEquals("You clicked: Cancel", $(RESULT).text());
    }

    @Test
    void promptAlertTest() {
        $(byText("Click for JS Prompt")).click();
        prompt("I am a JS prompt", "Test message");
        assertEquals("You entered: Test message", $(RESULT).text());
    }

    @Test
    void alertWithClickOptionsTest() {
        $(byText(CLICK_JS_CONFIRM)).click(usingJavaScript());
        confirm(JS_CONFIRM);
        assertEquals(CLICKED_OK, $(RESULT).text());
    }

    @Test
    void confirmAlertWithJsTest() {
        executeJavaScript("arguments[0].click()", $(byText(CLICK_JS_CONFIRM)));
        confirm(withExpectedText(JS_CONFIRM));
        assertEquals(CLICKED_OK, executeJavaScript("return arguments[0].textContent", $(RESULT)));
    }
}
