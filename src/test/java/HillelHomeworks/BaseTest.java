package HillelHomeworks;

import com.codeborne.selenide.Configuration;

public class BaseTest {
    static {
        Configuration.baseUrl = "https://the-internet.herokuapp.com";
        Configuration.timeout = 10000;
    }
}
